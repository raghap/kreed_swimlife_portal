import React, { Component } from 'react';
import { render } from 'react-dom'

import Master from './containers/Master'
import App from './containers/App'
import Fireadmin from './containers/Fireadmin'
import BatchAdmin from './containers/BatchAdmin'
import StaffAdmin from './containers/StaffAdmin'
import AthleteAdmin from './containers/AthleteAdmin'
import KsaAthlete from './containers/KsaAthlete'
import KsaCoach from './containers/KsaCoach'
import CoachAdmin from './containers/CoachAdmin'
import Attendence from './containers/Attendence'
import Settings from './containers/Settings'
import Reports from './containers/Reports'
import Logout from './containers/Logout'
import ChangePassword from './containers/ChangePassword'

import Push from './containers/Push'
import Config from   './config/app';

import { Router, Route,hashHistory,IndexRoute } from 'react-router'

class Admin extends Component {

  //Prints the dynamic routes that we need for menu of type fireadmin
  getFireAdminRoutes(item){
    if(item.link=="fireadmin"){
      return (<Route path={"/fireadmin/"+item.path} component={Fireadmin}/>)
    }else{

    }
  }

  //Prints the dynamic routes that we need for menu of type fireadmin
  getFireAdminSubRoutes(item){
    if(item.link=="fireadmin"){
      return (<Route path={"/fireadmin/"+item.path+"/:sub"} component={Fireadmin}/>)
    }else{

    }
  }

  //Prints the Routes
  /*
  {Config.adminConfig.menu.map(this.getFireAdminRoutes)}
  {Config.adminConfig.menu.map(this.getFireAdminSubRoutes)}
  */
  render() {
    return (
      <Router history={hashHistory}>
          <Route path="/" component={Master}>
            {/* make them children of `Master` */}
            <IndexRoute component={App}></IndexRoute>
            <Route path="/app" component={App}/>
            <Route path="/push" component={Push}/>

            <Route path="/fireadmin" component={Fireadmin}/>
            <Route path="/fireadmin/:sub" component={Fireadmin}/>

            <Route path="/BatchAdmin" component={BatchAdmin}/>
            <Route path="/BatchAdmin/:sub" component={BatchAdmin}/>

            <Route path="/CoachAdmin" component={CoachAdmin}/>
            <Route path="/CoachAdmin/:sub" component={CoachAdmin}/>

            <Route path="/AthleteAdmin" component={AthleteAdmin}/>
            <Route path="/AthleteAdmin/:sub" component={AthleteAdmin}/>

            <Route path="/ByCoaches" component={AthleteAdmin}/>
            <Route path="/ByCoaches/:sub" component={AthleteAdmin}/>

            <Route path="/ByBatches" component={AthleteAdmin}/>
            <Route path="/ByBatches/:sub" component={AthleteAdmin}/>


            <Route path="/StaffAdmin" component={StaffAdmin}/>
            <Route path="/StaffAdmin/:sub" component={StaffAdmin}/>

            <Route path="/KsaAthlete" component={KsaAthlete}/>
            <Route path="/KsaAthlete/:sub" component={KsaAthlete}/>

            <Route path="/KsaCoach" component={KsaCoach}/>
            <Route path="/KsaCoach/:sub" component={KsaCoach}/>

            <Route path="/Attendence" component={Attendence}/>
            <Route path="/Attendence/:sub" component={Attendence}/>
            
            <Route path="/Settings" component={Settings}/>
            <Route path="/Settings/:sub" component={Settings}/>

            <Route path="/Reports" component={Reports}/>
            <Route path="/Reports/:sub" component={Reports}/>

            <Route path="/ChangePassword" component={ChangePassword}/>
            <Route path="/ChangePassword/:sub" component={ChangePassword}/>

            <Route path="/Logout" component={Logout}/>
            <Route path="/Logout/:sub" component={Logout}/>

          </Route>
        </Router>
    );
  }

}

export default Admin;
