import React, {Component,PropTypes} from 'react'
import firebase from '../../config/database'
import FlatIndicator from '../FlatIndicator'
import AvatarImageCropper from 'react-avatar-image-cropper';

class KreedImage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value:props.value,
        isLoading:false,
        actionButtons:[<button className="btn btn-danger button-cancel" type='button' key={0}><i className="material-icons">cancel</i></button>,
                     <button className="btn btn-success button-submit" type='button' key={1}><i className="material-icons">check_circle</i></button>]
    };
    this.handleChange=this.handleChange.bind(this);
    this.submitImageToFirebase=this.submitImageToFirebase.bind(this);
    this.saveImageLinkInFirebase=this.saveImageLinkInFirebase.bind(this);
  }

  saveImageLinkInFirebase(link){
    this.setState({isLoading:false});
    this.props.updateAction(this.props.theKey,link);
  }

  submitImageToFirebase(value){
    var _this=this;
    // Create a root reference
    var storageRef = firebase.app.storage().ref('photos');
    var refFile=new Date().getTime()+".jpg"

    // Create a reference to 'mountains.jpg'
    var newImageRef = storageRef.child(refFile);
    var stripedImage=value.substring(value.indexOf('base64')+7, value.length);

    newImageRef.putString(stripedImage, 'base64').then(function(snapshot) {
      console.log('Uploaded a base64 string!');
      _this.saveImageLinkInFirebase(snapshot.downloadURL);
    });
  }

  handleChange(file) {
    this.setState({isLoading:true});
    if(this.props.setLoading)
      this.props.setLoading(true);

    //alert("Start processing ....")

    let reader = new FileReader();
    //let file = e.target.files[0];

    reader.onloadend = () => {
      console.log("Image is in base 64 now.. Upload it");
      this.setState({
        file: file,
        value: reader.result
      });
      this.submitImageToFirebase(reader.result)

    }

    reader.readAsDataURL(file)
  }

  render() {
    // var imgSrc=this.state.value&&this.state.value.length>4?this.state.value:"../../assets/img/image_placeholder.jpg";

    return (
      <div className="profileImg background-no-repeat" style={{  backgroundImage: "url(" +  this.state.value + ")", border: '1px solid black' , backgroundRepeat: "noRepeat" }}>
          <AvatarImageCropper actions={this.state.actionButtons} apply={this.handleChange} isBack={true} text={this.props.text} />
          <FlatIndicator show={this.state.isLoading} />
      </div>
    )
  }
}
export default KreedImage;

KreedImage.propTypes = {
    btnText: PropTypes.string,
    updateAction:PropTypes.func.isRequired,
    setLoading:PropTypes.func,
    theKey: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    class: PropTypes.string,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
};
