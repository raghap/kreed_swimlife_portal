import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
require("firebase/firestore");


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      num_athletes: '',
      num_coaches: '',
      num_staff: 2,
      num_batches: 2,
      num_ksa_athletes: 6,
      num_ksa_coaches: 1
    }
    this.getData = this.getData.bind(this);

  }
  //to fetch or get data from the firsbase database
  getData() {
    var _this = this;
    var userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + userId);

    docRef.get().then(doc => {
      var pathdoc = doc.data();
      db.doc(pathdoc.dbpath).get().then(snap => {
        _this.setState({
          num_athletes: snap.data().num_athletes ? snap.data().num_athletes : 0,
          num_coaches: snap.data().num_coaches ? snap.data().num_coaches : 0
        });
      });
    });
  }


  componentDidMount() {
    this.getData();
    window.sidebarInit();

    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy

  }

  render() {
    return (
      <div className="content">
        <NavBar>
          <a className="navbar-brand dashboard">Dashboard</a>
          <div className="pull-right">
            <a href="http://swimlife.org/" target="_blank"> <img src='assets/img/SwimLife_logo.png' /></a>
          </div>
        </NavBar>
        <div className="row">
          {/* number of athletes */}
          <Link to = "/AthleteAdmin/athletes">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">pool</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">Athletes</p>
                <h3 className="card-title">{this.state.num_athletes}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of athletes */}
          {/* number of coaches */}
          <Link to = "/CoachAdmin/coaches">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">person</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">Coaches</p>
                <h3 className="card-title">{this.state.num_coaches}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of coaches */}
          {/* number of Staff */}
          <Link to = "/StaffAdmin/staff">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">directions_walk</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">Staff</p>
                <h3 className="card-title">{this.state.num_staff}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of staff */}
          {/* number of Batches */}
          <Link to = "/BatchAdmin/batches">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">directions_walk</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">Batches</p>
                <h3 className="card-title">{this.state.num_batches}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of Batches */}
          {/* number of ksa athletes */}
          <Link to = "/KsaAthlete/athletes">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">pool</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">KSA Affiliated: Athletes</p>
                <h3 className="card-title">{this.state.num_ksa_athletes}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of ksa athletes */}
          {/* number of ksa coaches */}
          <Link to = "/KsaAthlete/coaches">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">person</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">KSA Affiliated: Coaches</p>
                <h3 className="card-title">{this.state.num_ksa_coaches}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of ksa coaches */}
        </div>
      </div>
    )
  }
}
export default App;
