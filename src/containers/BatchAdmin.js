import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Radio from '../components/fields/Radio.js'
import File from '../components/fields/File.js'
import Input from '../components/fields/Input.js';
import Table from '../components/tables/Table.js'
import DocsTable from '../components/tables/DocsTable.js'
import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import Image from '../components/fields/Image.js';
import KreedImage from '../components/fields/KreedImage.js';
import * as firebaseREF from 'firebase';
import KreedTimePicker from '../components/fields/KreedTimePicker.js';
import Select from '../components/fields/Select.js';
import SweetAlert from 'react-bootstrap-sweetalert';
import MultiSelect from 'react-select';
//import trim from 'trim';
require("firebase/firestore");

const ROUTER_PATH = "/batches/";
var Loader = require('halogen/PulseLoader');

class BatchAdmin extends Component {

  constructor(props) {
    super(props);


    //Create initial step
    this.state = {
      fields: {}, //The editable fields, textboxes, checkbox, img upload etc..
      arrays: {}, //The array of data
      elements: [], //The elements - objects to present
      elementsInArray: [], //The elements put in array
      directValue: "", //Direct access to the value of the current path, when the value is string
      firebasePath: "",
      arrayNames: [],
      currentMenu: {},
      completePath: "",
      lastSub: "",
      isJustArray: false,
      currentInsertStructure: null,
      notifications: [],
      lastPathItem: "",
      pathToDelete: null,
      isItArrayItemToDelete: false,
      page: 1,
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: false,
      showAddCollection: "test",
      debounce: false,
      userinfo: [],
      imageLoading: false,

      clubCoaches: [],
      clubAthletes: [],
      clubKsaAthletes:[],
      selectAllAthletes: [],
      selectCoaches: [],

      batch_name: "",
      coaches: "",
      athletes: "",
      description: "",
      from:"",
      to:"",

      photo: "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92",
      editedFields: [],
      errors: {},

      timingAlert: false,
      deleteAlert: false,
      selectAlert: false,

      // Multiple coach select
      selectedCoach: null,
      selectedCoachList:[],

      // Multiple Athletes select
      selectedAthletes: null,
      selectedAthletesList: [],

      // edit coach
      editSelectedCoach: null,
      editSelectedCoachList: [],
      selectedCoachesToEdit: [],

      //edit athlete
      editSelectedAthlete: null,
      editSelectedAthleteList: [],
      selectedAthletesToEdit: [],

      editName: ""
    };

    //Bind function to this

    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);
    this.addNewBatch = this.addNewBatch.bind(this);
    this.formValueCapture = this.formValueCapture.bind(this);
    this.ionViewDidLoad = this.ionViewDidLoad.bind(this);
    this.cancelAddNewBatch = this.cancelAddNewBatch.bind(this);
    this.saveEdits = this.saveEdits.bind(this);
    this.resetEdits = this.resetEdits.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.markUploaderStart = this.markUploaderStart.bind(this);
    this.apply = this.apply.bind(this);
    this.validatePhoto = this.validatePhoto.bind(this);

    //Alerts related
    this.hideAlert = this.hideAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);

    //handle changes for multiple select
    this.handleCoachChange = this.handleCoachChange.bind(this);
    this.handleAthleteChange = this.handleAthleteChange.bind(this);
    this.handleEditCoach = this.handleEditCoach.bind(this);
    this.handleEditAthlete = this.handleEditAthlete.bind(this);

    //edit coaches in edit page
    this.editCoaches = this.editCoaches.bind(this);
    this.cancelEditCoaches = this.cancelEditCoaches.bind(this);
    this.saveEditCoaches = this.saveEditCoaches.bind(this);

    //edit Athletes in edit page
    this.editAthletes = this.editAthletes.bind(this);
    this.cancelEditAthletes = this.cancelEditAthletes.bind(this);
    this.saveEditAthletes = this.saveEditAthletes.bind(this);
  }

   //Handle changes for multiple selection
  // Multiple Selection of Coach
  handleCoachChange = (selectedCoach) => {
    var mycoach=[];
      for(var coach of selectedCoach)
        // mycoach.push({reference_link :coach.value, coach_name :coach.label});
        mycoach.push(coach.label);
      this.setState({ selectedCoach,selectedCoachList: mycoach })
  }

  // Multiple selection of athlete
  handleAthleteChange = (selectedAthletes) => {
    var myathlete= [];
      for(var athlete of selectedAthletes)
        // myathlete.push({reference_link :athlete.value, athlete_name :athlete.label});
        myathlete.push(athlete.label);
      this.setState({ selectedAthletes,selectedAthletesList: myathlete })
  }

  // edit Coaches
  handleEditCoach = (editSelectedCoach) => {
    // var mycoach = []; 
    var mycoach= this.state.selectedCoachesToEdit;
      for(var coach of editSelectedCoach)
        // mycoach.push({reference_link :coach.value, coach_name :coach.label});
        mycoach.push(coach.label);
      this.setState({ editSelectedCoach,editSelectedCoachList: mycoach })
  }  

  // edit Athletes
  handleEditAthlete = (editSelectedAthlete) => {
    // var mycoach = []; 
    var myathlete= this.state.selectedAthletesToEdit;
      for(var athlete of editSelectedAthlete)
        // mycoach.push({reference_link :coach.value, athlete_name :coach.label});
        myathlete.push(athlete.label);
      this.setState({ editSelectedAthlete,editSelectedAthleteList: myathlete })
  }
  //End Handle changes for multiple selection

  //edit Athletes in edit page
  editAthletes(){
    // to get all ready assigned athletes
    var db = firebase.app.firestore();
    var cID = this.state.firebasePath.split('/')[5];
    var collection = "aquaticsID/India/centres/jywvMrL7tSLHRrRufVTu/batches"
    var _this = this;
    var docRef = db.collection(collection).doc(cID);
    var athletesList  = [];

    docRef.get().then(function(doc) {
        if (doc.exists) {
          var vals = doc.data().athletes;
          for(var i=0;i<vals.length;i++){
            _this.state.selectedAthletesToEdit.push(vals[i])
          }
          for(var j=0;j<_this.state.selectAllAthletes.length;j++){
            if( !(_this.state.selectedAthletesToEdit.includes(_this.state.selectAllAthletes[j].label))){
              athletesList.push(_this.state.selectAllAthletes[j]);
            }
          }
          _this.setState({selectAllAthletes : athletesList})
          _this.refs.editAthletesDialog.show();

        } else {
          console.log("No such document!");
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });

    // this.refs.editAthletesDialog.show();
  }

  //cancel edit athlete
  cancelEditAthletes(){
    var newState = {};

    newState.editSelectedAthlete = "";
    newState.editSelectedAthleteList = [];
    newState.selectedAthletesToEdit = [];

    this.setState(newState);
    this.refs.editAthletesDialog.hide();
  }

  //save edit athelte
  saveEditAthletes(e){
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */

    var db = firebase.app.firestore();
      if(this.state.editSelectedAthleteList == ""){
        this.showAlert("selectAlert");
        this.state.errorStatement = "Please ensure you have selected atleast one athlete";
        return;
      }
   
      var cID = this.state.firebasePath.split('/')[5];
      // var collection = this.state.firebasePath;
      var collection = "aquaticsID/India/centres/jywvMrL7tSLHRrRufVTu/batches"
      if (!this.state.debounce) {
        this.state.debounce = true;

        db.collection(collection).doc(cID).set({
          athletes: this.state.editSelectedAthleteList
          }, { merge: true })
          .then(docRef => {
            //cleanup and reset state
            this.refs.editAthletesDialog.hide();
            this.refs.addCollectionDialog.hide()
            this.refreshDataAndHideNotification()
            this.setState({ debounce: false, notifications: [{ type: "success", content: "Athletes added" }] })
          })
          .catch(function (error) {
            alert("Error adding document: " + error);
          });
      }
    this.refs.editAthletesDialog.hide();
  }
  //end edit Athletes in edit page

  //edit coaches in edit page
  editCoaches(){
    // to get all ready assigned coaches
    var db = firebase.app.firestore();
    var cID = this.state.firebasePath.split('/')[5];
    var collection = "aquaticsID/India/centres/jywvMrL7tSLHRrRufVTu/batches"
    var _this = this;
    var docRef = db.collection(collection).doc(cID);
    var coachesList  = [];
    docRef.get().then(function(doc) {
        if (doc.exists) {
          var vals = doc.data().coaches;
          for(var i=0;i<vals.length;i++){
            _this.state.selectedCoachesToEdit.push(vals[i])
          }

        // alert(_this.state.selectedCoachesToEdit)
        for(var j=0;j<_this.state.selectCoaches.length;j++){
          // alert(_this.state.selectCoaches[j].label)
          if( !(_this.state.selectedCoachesToEdit.includes(_this.state.selectCoaches[j].label))){
            coachesList.push(_this.state.selectCoaches[j]);
          }
        }
        // _this.state.selectCoaches = coachesList;
        _this.setState({selectCoaches : coachesList})
        _this.refs.editCoachesDialog.show();
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });

    // this.refs.editCoachesDialog.show();
  }

  //cancel edit coaches
  cancelEditCoaches(){
    var newState = {};

    newState.editSelectedCoach = "";
    newState.editSelectedCoachList = [];
    newState.selectedCoachesToEdit = [];

    this.setState(newState);
    this.refs.editCoachesDialog.hide();
  }

  //save edit coaches
  saveEditCoaches(e){
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */

    var db = firebase.app.firestore();
    if(this.state.editSelectedCoachList == ""){
      this.showAlert("selectAlert");
      this.state.errorStatement = "Please ensure you have selected atleast one coach";
      return;
    }
   
    var cID = this.state.firebasePath.split('/')[5];
    // var collection = this.state.firebasePath;
     var collection = "aquaticsID/India/centres/jywvMrL7tSLHRrRufVTu/batches"
    if (!this.state.debounce) {
      this.state.debounce = true;

      db.collection(collection).doc(cID).set({
        coaches: this.state.editSelectedCoachList
        }, { merge: true })
        .then(docRef => {
          //cleanup and reset state
          this.refs.editCoachesDialog.hide();
          this.refreshDataAndHideNotification()
          this.setState({ debounce: false, notifications: [{ type: "success", content: "Coaches added" }] })
        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });
    }
    this.refs.editCoachesDialog.hide();
  }
   //end edit coaches in edit page

  //sweet alerts for delete data
  showAlert(name) {
    this.setState({ [name]: true });
  }
  hideAlert(name) {
    this.setState({ [name]: false });
  }

  apply(file) {
    var src = window.URL.createObjectURL(file);
  }

  ionViewDidLoad() {
    this.userId = firebase.auth().currentUser.uid
    alert(this.userId);
  }

  //ionViewDidLoad();
  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
    window.sidebarInit();

  }

  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;

    newState.photo = "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92";
    newState.imageLoading = false;
    newState.editedFields = [];

    newState.batch_name = "";
    newState.coaches = "";
    newState.athletes = "";
    newState.description = "";
    newState.from = "";
    newState.to = "";

    newState.clubCoaches = [];
    newState.clubAthletes = [];
    newState.clubKsaAthletes = [];

    newState.selectedCoach = [];
    newState.selectCoaches = [];
    newState.selectedCoachList = [];

    newState.selectedAthletes = []
    newState.selectedAthletesList = [];

    newState.editSelectedCoach = "";
    newState.editSelectedCoachList = "";

    newState.editSelectedAthlete = "";
    newState.editSelectedAthleteList = "";
    newState.selectedCoachesToEdit = [];
    newState.selectedAthletesToEdit = [];

    //newState.editedFields = [];
    this.resetEdits();

    this.setState(newState);
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
  }

  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
    this.state.selectCoaches = [];
    
    this.userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    
    db.collection("centremaps").doc("user" + this.userId).get()
    .then(doc => {
        this.state.centrePath = doc.data().dbpath;
        return db.collection("clubmaps").doc("user" + this.userId).get()
    })
    .then(docref => {
        this.state.ksaPath = docref.data().dbpath;
        this.cacheCoachesAndAthletes();
    })
    .catch(error => {
        console.log("Fatal Error: "+error);
    })
  }

  //caching coaches and athletes details
  cacheCoachesAndAthletes()
  {
    var db = firebase.app.firestore();

    //Get Everything 
    db.collection(this.state.centrePath + "/coaches").get()
    .then(qss1 => {
      this.state.clubCoaches = qss1.docs;
      return db.collection(this.state.centrePath + "/athletes").get()
    })
    .then(qss2 => {
      this.state.clubAthletes = qss2.docs;
      return db.collection(this.state.ksaPath + "/athletes").get()
    })
    .then(qss3 => {
      this.state.clubKsaAthletes = qss3.docs;
      return db.collection(this.state.ksaPath + "/coaches").get()
    })
    .then(qss4 => {
      this.state.clubKsaCoaches = qss4.docs;

      //Populate select contrl data
      for(var cdoc of this.state.clubCoaches)
        this.state.selectCoaches.push({value: cdoc.ref.path, label: cdoc.data().name});
      for(var adoc of this.state.clubAthletes)
      // alert("Id " + adoc.ref.id + ",name " + adoc.data().full_name + ",ref " +  adoc.data());
        this.state.selectAllAthletes.push({value: adoc.ref.path, label: adoc.data().full_name});
      for(var kdoc of this.state.clubKsaAthletes)
        this.state.selectAllAthletes.push({value: kdoc.ref.path, label: kdoc.data().full_name});
      for(var kdoc of this.state.clubKsaCoaches)
        this.state.selectCoaches.push({value: kdoc.ref.path, label: kdoc.data().name});

      
      //resume normal operation
      this.findFirestorePath(this.state.centrePath);
    })
    .catch(error => {
      alert("Error" + error)
    })
  }
  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath(firebasePath) {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    //var firebasePath= this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath + "/" + pathData.lastSub;
    pathData.firebasePath = pathData.firebasePath.replace(/\+/g, "/");
    pathData.lastSub = pathData.firebasePath.replace(/\//g, "+");

    //Find last path - the last item
    var subPath = pathData.lastSub;//this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(pathData.firebasePath);
    // this.getCollectionDataFromFireStore(firebasePath);
  }

  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
  getCollectionDataFromFireStore(collection) {

    //Create the segmments based on the path / collection we have
    var segments = collection.split("/");
    var lastSegment = segments[segments.length - 1];

    //Is this a call to a collections data
    var isCollection = segments.length % 2;

    //Reference to this
    var _this = this;

    //Save know info for now
    this.setState({
      currentCollectionName: segments[segments.length - 1],
      isCollection: isCollection,
      isDocument: !isCollection,
    })

    //Get reference to firestore
    var db = firebase.app.firestore();

    //Here, we will save the documents from collection
    var documents = [];

    if (isCollection) {
      //COLLECTIONS - GET DOCUMENTS 

      db.collection(collection).orderBy("name").get()
        .then(function (querySnapshot) {
          var datacCount = 0;
          querySnapshot.forEach(function (doc) {

            //Increment counter
            datacCount++;

            //Get the object
            var currentDocument = doc.data();

            //Sace uidOfFirebase inside him
            currentDocument.uidOfFirebase = doc.id;

            console.log(doc.id, " => ", currentDocument);

            //Save in the list of documents
            documents.push(currentDocument)
          });
          console.log("DOCS----");
          console.log(documents);

          //Save the douments in the sate
          _this.setState({
            isLoading: false,
            documents: documents,
            showAddCollection: datacCount == 0 ? collection : ""
          })
          if (datacCount == 0) {
            _this.refs.addCollectionDialog.show();
          }
          console.log(_this.state.documents);
        });
    } else {
      //DOCUMENT - GET FIELDS && COLLECTIONS
      var referenceToCollection = collection.replace("/" + lastSegment, "");

      //Create reference to the document itseld
      var docRef = db.collection(referenceToCollection).doc(lastSegment);

      //Get the starting collectoin
      var parrentCollection = segments;
      parrentCollection.splice(-1, 1);

      //Find the collections of this document
      this.findDocumentCollections(parrentCollection);

      docRef.get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());

          //Directly process the data
          _this.processRecords(doc.data())
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }
  }

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {

    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {
    console.log(records);
    this.state.editName = records.name;
    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list

    console.log(Common.getClass(records));
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            //alert(key);
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    //pp_add
    var navigation = Config.navigation;
    var itemFound = false;
    var showFields = null;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].editFields && navigation[i].link == "BatchAdmin") {
        showFields = navigation[i].editFields;
        itemFound = true;
      }
    }
    //end pp_add

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)

    var pos = -1;

    keysFromFirebase.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        if (showFields)
          pos = showFields.indexOf(key);
        if (pos < 0)
          return;
        // fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        // fieldsAsArray.splice(pos, 0, { "theKey": key, "value": fields[key] })
        fieldsAsArray[pos] =  { "theKey": key, "value": fields[key] }
      }
    });



    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
 updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
  var subLink = this.state.theSubLink;
  if (byGivvenSubLink != null) {
    subLink = byGivvenSubLink;
  }
  console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
  var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
  var _this = this;
  //First get the document
  //DOCUMENT - GET FIELDS && COLLECTIONS
  var docRef = firebase.app.firestore().doc(firebasePath);
  docRef.get().then(function (doc) {
    if (doc.exists) {
      var numChunks = chunks.length - 1;
      var doc = doc.data();
      //("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {        
      if (value == "DELETE_VALUE") {
        if (numChunks == 2) {
          if (chunks[1] == "attachments") //special treatment for attachments
          {
            var docPath = doc[chunks[1]][chunks[2]];
            docPath = unescape(docPath.slice(1 + docPath.lastIndexOf('/'), docPath.lastIndexOf('?')));
            var dRef = firebase.app.storage().ref().child(docPath);

            // Delete the file
            dRef.delete().then(someval => {
              console.log("Deleted from storage file: ", docPath);
            }).catch(function (error) {
              console.log("Could not delete storage file: " + error);
            });

          }//end special treatment for attachments
          doc[chunks[1]].splice(chunks[2], 1);
        }
        if (numChunks == 1) {
          doc[chunks[1]] = null;
        }
      } else {
        //Normal update, or insert
        if (numChunks == 2) {
          doc[chunks[1]][chunks[2]] = value
        }
        if (numChunks == 1) {
          doc[chunks[1]][key] = value
        }
      }

      console.log("Document data:", doc);
      _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
      if (callback) {
        callback();
      }

      //alert(chunks.length-1);
      //_this.processRecords(doc.data())
      //console.log(doc);

    } else {
      console.log("No such document!");
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });

}
  //to save fields edited in edit page
  saveEdits() {
    var firebasePath = (this.props.route.path.replace("/BatchAdmin/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    var db = firebase.app.firestore();

    //var databaseRef = db.doc(firebasePath);
    var databaseRef = db.doc(this.state.firebasePath);
    var updateObj = {};
    var i;
    for (i = this.state.editedFields.length - 1; i >= 0; --i) {
      updateObj[this.state.editedFields[i]] = this.state[this.state.editedFields[i]];
      this.state.editedFields.splice(i, 1);
    }
    databaseRef.set(updateObj, { merge: true });

  }

  resetEdits() {
    this.state.editedFields.splice(0, this.state.editedFields.length);
  }

  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */

  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
 updateAction(key, value, dorefresh = true, type = null, forceObjectSave = false) {
  const pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
  value = this.processValueToSave(value, type);
  var firebasePath = (this.props.route.path.replace("/AthleteAdmin/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
  if (this.state.theSubLink != null && !forceObjectSave) {
    this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
  } else {

    //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
    //key="creator";

    console.log("firebasePath from update:" + firebasePath)
    console.log('Update ' + key + " into " + value);

    if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
      console.log("THE_NEW_KEY")
      var updateObj = {};
      updateObj[key] = value;
      this.setState(updateObj);
      console.log(updateObj);
    } else {
      if (forceObjectSave) //Maybe coming from a delete action
      {
        var db = firebase.app.firestore();

        //special treatment for attachments. Don't zero out the array
        if (key == "attachments" && !Array.isArray(value))
          value = [];

        var databaseRef = db.doc(this.state.firebasePath);
        var updateObj = {};
        updateObj[key] = value;
        databaseRef.set(updateObj, { merge: true });
      }
      else //PP: DONT UPDATE THE DB HERE. WAIT FOR THE SAVE
      {

        if (!this.state.editedFields.includes(key))
          this.state.editedFields.push(key);

        if (key == "photo") {
          this.state.imageLoading = false;
        }

        this.setState({ [key]: value });
      }
    }

  }
}

  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table bellow." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide();
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {
    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */

  deleteFieldAction(key, isItArrayItem = false, theLink = null) {
    console.log("Delete " + key);
    console.log(theLink);
    if (theLink != null) {
      theLink = theLink.replace("/BatchAdmin", "");
    }
    if (isNaN(key)) {
      isItArrayItem = false;
    }

    //special treatment when deleting attachments
    if (isItArrayItem)
      this.state.deleteAlertTitle = "This attachment will be deleted permanently";
    else
      this.state.deleteAlertTitle = "All data associated with this athlete will be deleted!";

    console.log("Is it array: " + isItArrayItem);
    var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (key != null) {
      //firebasePathToDelete+=("/"+key)
    }

    console.log("firebasePath for delete:" + firebasePathToDelete);
    this.setState({ deleteAlert: true, pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });

  }

  /**
  * doDelete - do the actual deleting based on the data in the state
  */
  doDelete() {
    var _this = this;

    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;

    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      // _this.refs.deleteDialog.hide();
      this.hideAlert("deleteAlert");
      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data
      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();
      if (chunks.length % 2) {

        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);
        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        //db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function() {
        db.collection(this.state.firebasePath).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, pathToDelete: null, notifications: [{ type: "success", content: "Athlete deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing athlete: ", error);
        });

      }

    }

  }


  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }



  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;

    var itemFound = false;
    var navigation = Config.navigation;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].path == "batches" && navigation[i].tableFields && navigation[i].link == "BatchAdmin") {
        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          if (navigation[i].subMenus[j].path == firebasePath && navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "StafAdmin") {
            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

//generate docs table in edit page
makeArrayCard(name) {
  return (
    <div className="col-md-12" key={name}>
      <div className="card">
        <div className="card-header card-header-icon" data-background-color="rose">
          <h4 className="card-title">Edit</h4>
          {/* <i className="material-icons">assignment</i> */}
        </div>
        {Common.capitalizeFirstLetter(name) == "Coaches" && <a style={{ cursor: "pointer" }} onClick={() => { this.editCoaches() }}>
        <div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
        <i className="material-icons">add</i>
        </div>
        </a>}
        {Common.capitalizeFirstLetter(name) == "Athletes" && <a style={{ cursor: "pointer" }} onClick={() => { this.editAthletes() }}>
        <div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
        <i className="material-icons">add</i>
        </div>
        </a>}
        <div className="card-content">
          <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
          <div className="toolbar">

          </div>
          <div className="material-datatables">
            <DocsTable
              isFirestoreSubArray={true}
              showSubItems={this.showSubItems}
              //headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
              headers={["name"]}
              deleteFieldAction={this.deleteFieldAction}
              fromObjectInArray={false} name={name}
              routerPath={this.props.route.path}
              isJustArray={this.state.isJustArray}
              sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
              data={this.state.arrays[name]} />
          </div>
        </div>
      </div>
    </div>
  )
}
  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  makeCollectionTable() {
    var name = this.state.currentCollectionName;

    for(var athlete of this.state.documents){   
      athlete.number_of_athletes = athlete.athletes.length;
    }
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          {/*begin pp_add*/}
          {/*
                  <a  onClick={()=>{this.addDocumentToCollection(name)}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}> */}
          <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show() }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            {/*end pp_add*/}

            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates single array section
   * @param {String} name, used as key also
   */


  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/BatchAdmin/";
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }


  formValueCapture(k, v) {
    //alert(k);
    this.setState({ [k]: v });
  }

  //Validate inputs
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    this.setState({
      [name]: value
    });
  }

  validatePhoto() {
    if (this.state.photo == "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92") {
      return false;
    }
    return true;

  }
  markUploaderStart() {
    this.state.imageLoading = true;
  }

  //cancel add new batch
  cancelAddNewBatch() {
    var newState = {};

    newState.batch_name = "";
    newState.coaches = "";
    newState.athletes = "";
    newState.description = "";
    newState.from = "";
    newState.to = "";

    newState.photo = "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92";
    newState.editedFields = [];
    newState.imageLoading = false;
    newState.selectedCoach = []
    newState.selectedCoachList = [];

    this.setState(newState);

    this.refs.addnewitemDialog.hide();
  }

  //addition of new batch
  addNewBatch(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
    
    var db = firebase.app.firestore();
    if(this.state.from == ""){
      this.showAlert("timingAlert");
      return;
    }
    if(this.state.to == ""){
      this.showAlert("timingAlert");
      return;
    }

    if(this.state.selectedCoachList == ""){
      this.showAlert("selectAlert");
      this.state.errorStatement = "Please ensure you have selected atleast one Coach";
      return;
    }

    var collection = this.state.firebasePath;
    if (!this.state.debounce) {
      this.state.debounce = true;

      db.collection(collection).add({
        name: this.state.batch_name,
        coaches: this.state.selectedCoachList,
        athletes: this.state.selectedAthletesList,
        description: this.state.description,
        from: this.state.from,
        to: this.state.to
      })
        .then(docRef => {
          //cleanup and reset state
          this.refs.addCollectionDialog.hide()
          this.refs.addnewitemDialog.hide()
          this.refreshDataAndHideNotification()
          this.setState({ debounce: false, notifications: [{ type: "success", content: "New Batch Added." }] })
        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });
    }
  }

  //MAIN RENDER FUNCTION 
  render() {
    const {selectedCoach} = this.state;
    const {selectedAthletes} = this.state;
    const {editSelectedCoach} = this.state;
    const {editSelectedAthlete} = this.state;
    // Styling for add new caoch skylight
    var addnewitemDialog = {
      width: '50%',
      height: 'auto',
      marginLeft: '-35%',
      position: 'absolute',
      padding: '0px',
      left: '60%'
    };
    var editCoachesDialog = {
      width: '50%',
      height: 'auto',
      marginLeft: '-35%',
      position: 'absolute',
      padding: '0px',
      left: '60%'
    }

    return (
      <div className="content">
        <NavBar>
          {this.state.isCollection && this.state.documents.length > 0 ? 
            <h4 style={{float :"left", paddingLeft:"10px"}}>Manage {Common.capitalizeFirstLetter(this.state.completePath.split("+")[4])}</h4> 
          : ""}
          
          {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? 
            <h4 style={{float :"left", paddingLeft:"10px"}}>Edit Batch : {this.state.editName}</h4>
          : ""}

          <div className="pull-right">
          <a href="http://swimlife.org/" target="_blank"> <img src='assets/img/SwimLife_logo.png' /></a>
          </div>
        </NavBar>


        <div className="content" sub={this.state.lastSub}>

          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

            {/* sweet photo alert */}
            <SweetAlert warning
              show={this.state.timingAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ timingAlert: false })}>
              Please ensure you have selected batch timings.
              </SweetAlert>

              <SweetAlert warning
              show={this.state.selectAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ selectAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>

            {/* sweet delete alert */}
            <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="default"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.setState({ deleteAlert: false })}
            >
              {this.state.deleteAlertTitle}
            </SweetAlert>

            {/* sweet delete alert */}
            {/* <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="default"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.setState({ deleteAlert: false })}
            >
              All data associated with this Batch will be deleted!
              </SweetAlert> */}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}
            
            {/* Documents in collection */}
            {this.state.isCollection && this.state.documents.length > 0 ? this.makeCollectionTable() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}


            {/* FIELDS */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">Edit</h4>
                    {/* <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4> */}
                  </div>
                  <h4 style = {{ display: "inline", paddingTop:"10px"}}>Basic Info</h4>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {
                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)
                  }) : ""}
                  <div
                    className="text-center">
                    <Link to='/BatchAdmin/batches'>
                      <button onClick={this.saveEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Save">Save</button>
                    </Link>
                    <Link to='/BatchAdmin/batches'>
                      <button
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Cancel">Cancel</button>
                    </Link>
                  </div>
                </form>
              </div>
            </div>) : ""}

            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">
                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/BatchAdmin/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>
                </form>
              </div>
            </div>) : ""}

            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">
                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>
                </form>
              </div>
            </div>) : ""}
          </div>
        </div>
        {/* Skylight for adding first coach */}
        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add the first BatchAdmin in your club</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no Batches yet. Add your first batch.</Notification>
          </div>

          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={() => { this.refs.addnewitemDialog.show() }} className="btn btn-success center-block">ADD</a>
            </div>

          </div>

        </SkyLight>
        {/* End Skylight for adding first coach */}
        {/* Skylight for adding new coach */}
        {/*begin pp_add*/}
        <SkyLight dialogStyles={addnewitemDialog} ref="addnewitemDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Add New Batch</h4>
              <form className="padding-10" onSubmit={this.addNewBatch}>
                <div className="row">
                <div className="col-sm-2"></div>
                  <div className="col-sm-8">
                    {/* Batch name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Batch Name :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="batch_name" aria-required="true" value={this.state.batch_name} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Batch name */}
                    {/* description */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Description :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <textarea rows="5" cols="30" required="true" name="description" aria-required="true" value={this.state.description} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* description */}
                    {/* From */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">From :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                          <KreedTimePicker theKey="from" value={this.state.from} updateAction={this.updateAction} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* From */}
                    {/* To */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">To :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                          <KreedTimePicker theKey="to" value={this.state.to} updateAction={this.updateAction} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* To */}
                    {/* coach name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Coaches :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <MultiSelect
                              value={selectedCoach}
                              onChange={this.handleCoachChange}
                              options={this.state.selectCoaches}
                              isMulti= {true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* coach name */}
                    {/* coach name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Athletes :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <MultiSelect
                              value={selectedAthletes}
                              onChange={this.handleAthleteChange}
                              options={this.state.selectAllAthletes}
                              isMulti= {true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* coach name */}
                  </div>
                  <div className="col-sm-2"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelAddNewBatch} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/*end pp_add*/}
        {/*End Skylight for adding new coach */}

        {/* Edit Athletes */}
        <SkyLight dialogStyles={editCoachesDialog} ref="editAthletesDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Edit Athletes</h4>
              <form className="padding-10" onSubmit={this.saveEditAthletes}>
                <div className="row">
                <div className="col-sm-2"></div>
                  <div className="col-sm-8">
                    {/* Athlete name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Select athletes :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <MultiSelect
                              value={editSelectedAthlete}
                              onChange={this.handleEditAthlete}
                              options={this.state.selectAllAthletes}
                              isMulti= {true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Athlete name */}
                  </div>
                  <div className="col-sm-2"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelEditAthletes} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/* End Edit athletes */}
          
        {/* Edit coaches */}
          <SkyLight dialogStyles={editCoachesDialog} ref="editCoachesDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Edit Coaches</h4>
              <form className="padding-10" onSubmit={this.saveEditCoaches}>
                <div className="row">
                <div className="col-sm-2"></div>
                  <div className="col-sm-8">
                    {/* coach name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Select coaches :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <MultiSelect
                              value={editSelectedCoach}
                              onChange={this.handleEditCoach}
                              options={this.state.selectCoaches}
                              isMulti= {true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* coach name */}
                  </div>
                  <div className="col-sm-2"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelEditCoaches} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/* End edit coaches */}

        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>        
      </div>
    )
  }
}
export default BatchAdmin;