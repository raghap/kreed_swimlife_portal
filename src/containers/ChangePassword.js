import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
import SweetAlert from 'react-bootstrap-sweetalert';
require("firebase/firestore");


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password:"",
      re_password:"",
      errorAlert: false,
      errorStatement:"",
      type: "password",
      password_view_status: "fa fa-eye"
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.submitPassword = this.submitPassword.bind(this);
    this.cancelSubmit = this.cancelSubmit.bind(this);
    this.showAlert = this.showAlert.bind(this);
    this.viewPassword = this.viewPassword.bind(this);
  }
  viewPassword(){
    if(this.state.type == "password" && this.state.password_view_status == "fa fa-eye"){
      this.setState({type:"text" , password_view_status:"fa fa-eye-slash"});
    }
    else if(this.state.type == "text" && this.state.password_view_status == "fa fa-eye-slash"){
      this.setState({type:"password" , password_view_status:"fa fa-eye"});
    }
  }
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
  

    this.setState({
      [name]: value
    });
  }
  submitPassword(e){
    e.preventDefault(); // <- prevent form submit from reloading the page
    if(this.state.password != this.state.re_password){
      this.showAlert("errorAlert");
      this.state.errorStatement = "Password does not match";
      this.state.password = "";
      this.state.re_password = "";
      return;
    }
    alert("PASSWORD CHANGED")
  }
  cancelSubmit(){
    var newState = {};

    newState.password = "";
    newState.re_password = "";
    this.setState(newState);

  }

  showAlert(name) {
    this.setState({ [name]: true });
  }

  componentDidMount() {
    this.getData();
    window.sidebarInit();

    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy

  }

  render() {
    return (
      <div className="content">
        <NavBar>
          <h4 style={{float :"left", paddingLeft:"10px"}}>Manage Password</h4> 
          <div className="pull-right">
            <a href="http://swimlife.org/" target="_blank"> <img src='assets/img/SwimLife_logo.png' /></a>
          </div>
        </NavBar>
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
        </SweetAlert>
        <div className="row">
            <div className="col-sm-2"></div>
            <div className="col-sm-8">
            <form className="padding-10" onSubmit={this.submitPassword}>
                {/* Password */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Enter Password :</label>
                    </div>
                    <div className="col-sm-4 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type={this.state.type} required="true" name="password" aria-required="true" value={this.state.password} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                {/* Password */}
                {/* Password */}
                <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Re-enter Password :</label>
                    </div>
                    <div className="col-sm-4 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type={this.state.type} required="true" name="re_password" aria-required="true" value={this.state.re_password} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-1">
                     <a style={{ cursor: "pointer" }}  onClick={this.viewPassword}>
                        <i style={{ fontSize: "20px",color: "black" }}  className={this.state.password_view_status} ></i>
                     </a>
                    </div>
                  </div>
                {/* Password */}
                <div className="row margin-top-25px">
                  <div className="col-sm-10 text-align-center">
                    <button className="btn btn-rose btn-size font-size password-btn" value="Save"> <i className="material-icons">save</i> Submit</button>
                    <button onClick={this.cancelSubmit} className="btn btn-rose btn-size font-size password-btn" value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                  </div>
              </div>
            </form>
            </div>
            <div className="col-sm-2"></div>
        </div>
      </div>
    )
  }
}
export default App;
