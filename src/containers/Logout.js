import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
import SweetAlert from 'react-bootstrap-sweetalert';
require("firebase/firestore");


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      num_athletes: '',
      num_coaches: '',
      deleteAlert: true,
    }
    this.getData = this.getData.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  
  handleLogout() {
    // e.preventDefault();

    console.log('The link was clicked.');

    firebase.app.auth().signOut();
    window.closeSideBar();
  }
  //to fetch or get data from the firsbase database
  getData() {
    var _this = this;
    var userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + userId);

    docRef.get().then(doc => {
      var pathdoc = doc.data();
      db.doc(pathdoc.dbpath).get().then(snap => {
        _this.setState({
          num_athletes: snap.data().num_athletes ? snap.data().num_athletes : 0,
          num_coaches: snap.data().num_coaches ? snap.data().num_coaches : 0
        });
      });
    });
  }


  componentDidMount() {
    this.getData();
    window.sidebarInit();

    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy

  }

  render() {
    return (
      <div className="content">
        <NavBar>
          <h4 style={{float :"left", paddingLeft:"10px"}}>Logout</h4> 
          <div className="pull-right">
            <a href="http://swimlife.org/" target="_blank"> <img src='assets/img/SwimLife_logo.png' /></a>
          </div>
        </NavBar>
          {/* sweet delete alert */}
          <SweetAlert
            warning
            showCancel
            show={this.state.deleteAlert}
            confirmBtnText="Yes"
            confirmBtnBsStyle="danger"
            confirmBtnCssClass="deleteAlertBtnColor"
            cancelBtnBsStyle="default"
            title="Are you sure?"
            onConfirm={() => this.handleLogout()}
            onCancel={() => this.setState({ deleteAlert: false })}
          >
           You want to Logout?
            </SweetAlert>
      
      </div>
    )
  }
}
export default App;
