import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
require("firebase/firestore");


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      num_athletes: '',
      num_coaches: ''
    }
    this.getData = this.getData.bind(this);

  }
  //to fetch or get data from the firsbase database
  getData() {
    var _this = this;
    var userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + userId);

    docRef.get().then(doc => {
      var pathdoc = doc.data();
      db.doc(pathdoc.dbpath).get().then(snap => {
        _this.setState({
          num_athletes: snap.data().num_athletes ? snap.data().num_athletes : 0,
          num_coaches: snap.data().num_coaches ? snap.data().num_coaches : 0
        });
      });
    });
  }


  componentDidMount() {
    this.getData();
    window.sidebarInit();

    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy

  }

  render() {
    return (
      <div className="content">
        <NavBar>
        <h4 style={{float :"left", paddingLeft:"10px"}}>Reports</h4>
          <div className="pull-right">
            <a href="http://swimlife.org/" target="_blank"> <img src='assets/img/SwimLife_logo.png' /></a>
          </div>
        </NavBar>
        <div className="row">

        {/*All athletes */}
        <Link to = "/AthleteAdmin/athletes">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">pool</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">All Athletes</p>
                {/* <h3 className="card-title">40</h3> */}
              </div>
            </div>
          </div>
         </Link>
        {/*All athletes */}

        {/*athletes By Coaches */}
        <Link to = "/ByCoaches/athletes">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">record_voice_over</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">By Coaches</p>
                {/* <h3 className="card-title">40</h3> */}
              </div>
            </div>
          </div>
         </Link>
        {/* athletes By Coaches */}
        {/*athletes By Batches*/}
        <Link to = "/ByBatches/athletes">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">group</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">By Batches</p>
                {/* <h3 className="card-title">40</h3> */}
              </div>
            </div>
          </div>
         </Link>
        {/*athletes By Batches*/}
        </div>


       
      </div>
    )
  }
}
export default App;
