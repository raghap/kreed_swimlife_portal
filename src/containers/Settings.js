import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
require("firebase/firestore");


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      num_athletes: '',
      num_coaches: ''
    }
    this.getData = this.getData.bind(this);

  }
  //to fetch or get data from the firsbase database
  getData() {
    var _this = this;
    var userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + userId);

    docRef.get().then(doc => {
      var pathdoc = doc.data();
      db.doc(pathdoc.dbpath).get().then(snap => {
        _this.setState({
          num_athletes: snap.data().num_athletes ? snap.data().num_athletes : 0,
          num_coaches: snap.data().num_coaches ? snap.data().num_coaches : 0
        });
      });
    });
  }


  componentDidMount() {
    this.getData();
    window.sidebarInit();

    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy

  }

  render() {
    return (
      <div className="content">
        <NavBar>
          <div className="pull-right">
            <a href="http://swimlife.org/" target="_blank"> <img src='assets/img/SwimLife_logo.png' /></a>
          </div>
        </NavBar>
          <div className="row">
            <p className="coming-soon-styling">Coming Soon...</p>
          </div>
        </div>
    )
  }
}
export default App;
